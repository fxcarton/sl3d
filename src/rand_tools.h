#ifndef RAND_TOOLS_H
#define RAND_TOOLS_H

float rand_float(float min, float max);
void gen_perlin(float* array, int size, int resol,
        float amplitude, float exponent, int additive);
void gen_diamond_square(float* array, const float* pondmap, int size);
int bernoulli(float prob);

#endif
