#include <3dmr/scene/node.h>
#include "map.h"

#ifndef MINIMAP_H
#define MINIMAP_H

struct Node* minimap_new(const float* heightMap, unsigned int size);
void minimap_free(struct Node* minimap);

struct Node* minimap_add_point(struct Node* minimap, float r, float g, float b);
void minimap_move_point(const struct Map* map, struct Node* point, float x, float z);

#endif
