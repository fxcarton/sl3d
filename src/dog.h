#ifndef DOG_H
#define DOG_H

#include <3dmr/animation/animation.h>
#include "map.h"
#include "sled.h"

enum DogUpdateError {
    DOG_NO_ERROR,
    DOG_SLED_OUT_OF_MAP = SLED_OUT_OF_MAP,
    DOG_SLED_TOO_MUCH_ROTATION = SLED_TOO_MUCH_ROTATION,
    DOG_SLED_COLLISION_WITH_TREE = SLED_COLLISION_WITH_TREE,
    DOG_OUT_OF_MAP,
    DOG_COLLISION_WITH_TREE,
    DOG_COLLISION_WITH_SLED
};

enum PullType {
    PULL_NONE,
    PULL_DOG,
    PULL_SLED
};

enum DogStatus {
    DOG_IDLE = 0,
    DOG_WALKING,
    DOG_RUNNING,
    DOG_NB_STATUS
};

struct Dog {
    enum DogStatus status;
    struct Node nodes[6];
    struct Geometry geoms[5];
    struct Animation anims[DOG_NB_STATUS][5];
    struct PhongMaterialParams phongParams;
    float animt;
    float forceNorm;
    Vec3 traction;
    Vec3 goal;

    enum PullType pullType;
    void* pulledObject;
};

struct Dog* dog_new(enum PullType pullType, void* pulledObject);

void dog_incr_force(struct Dog* dog);
void dog_decr_force(struct Dog* dog);
void dog_rotate(struct Dog* dog, float angle);

enum DogUpdateError dog_update(struct Dog* dog, struct Map* map, float dt);

void dog_free(struct Dog* dog);

#endif

