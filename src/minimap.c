#include <stdio.h>
#include <stdlib.h>

#include <3dmr/material/solid.h>
#include <3dmr/render/texture.h>
#include <3dmr/scene/node.h>
#include "map.h"
#include "minimap.h"

static float vertices[] = {
    /* position    |    TC  */
    0.0, 0.0, 0.0,   0.0, 0.0,
    0.0, 0.2, 0.0,   1.0, 0.0,
    0.2, 0.0, 0.0,   0.0, 1.0,
    0.2, 0.0, 0.0,   0.0, 1.0,
    0.0, 0.2, 0.0,   1.0, 0.0,
    0.2, 0.2, 0.0,   1.0, 1.0
};

struct Node* minimap_new(const float* heightMap, unsigned int size) {
    struct Mesh mesh;
    GLuint texture;
    unsigned char* buffer;
    unsigned char* ptr;
    unsigned int x, y;
    unsigned int size2 = size + 1;
    float cur, min = heightMap[0], max = heightMap[0];
    Vec3 t = {0.7, 0.7, 0.0};
    struct Node* ret;
    struct SolidMaterialParams* mparams;
    struct Geometry* geom;

    if (!(ret = malloc(sizeof(*ret) + sizeof(*mparams) + sizeof(*geom)))) {
        return NULL;
    }
    mparams = (void*)(ret + 1);
    geom = (void*)(mparams + 1);
    node_init(ret);

    if (!(buffer = malloc(size * size * 3))) {
        fprintf(stderr, "Warning: minimap allocation failed\n");
        free(ret);
        return NULL;
    }

    for (y = 0; y < size; y++) {
        for (x = 0; x < size; x++) {
            cur = heightMap[y * size2 + x];
            if (cur > max) {
                max = cur;
            } else if (cur < min) {
                min = cur;
            }
        }
    }

    ptr = buffer;
    for (y = 0; y < size; y++) {
        for (x = 0; x < size; x++) {
            cur = (heightMap[y * size2 + x] - min) / (max - min);
            *ptr++ = 255.0 * cur;
            *ptr++ = 255.0 * cur;
            *ptr++ = 108.0 * cur;
        }
    }

    texture = texture_load_from_uchar_buffer(buffer, size, size, 3, 0);
    free(buffer);
    solid_material_params_init(mparams);
    material_param_set_vec3_texture(&mparams->color, texture);

    mesh.numVertices = 6;
    mesh.numIndices = 0;
    mesh.vertices = vertices;
    mesh.indices = NULL;
    mesh.flags = MESH_TEXCOORDS;
    geom->vertexArray = vertex_array_new(&mesh);
    geom->material = NULL;

    if (!texture || !geom->vertexArray || !(geom->material = solid_overlay_material_new(mesh.flags, mparams))) {
        minimap_free(ret);
        return NULL;
    }

    node_set_geometry(ret, geom);
    node_translate(ret, t);
    ret->alwaysDraw = 1;
    return ret;
}

static void minimap_point_free(struct Node*);

static void minimap_nodes_free(struct Node* node) {
    if (node->alwaysDraw) minimap_point_free(node);
}

void minimap_free(struct Node* minimap) {
    if (minimap) {
        struct SolidMaterialParams* mparams = (void*)(minimap + 1);
        struct Geometry* geom = (void*)(mparams + 1);
        GLuint texture = mparams->color.value.texture;
        if (texture) glDeleteTextures(1, &texture);
        vertex_array_free(geom->vertexArray);
        free(geom->material);
        minimap->alwaysDraw = 0;
        if (minimap->nbChildren) nodes_free(minimap, minimap_nodes_free);
        free(minimap);
    }
}

struct Node* minimap_add_point(struct Node* minimap, float r, float g, float b) {
    float vertices[18];
    struct Mesh mesh;
    struct Node* ret;
    struct SolidMaterialParams* mparams;
    struct Geometry* geom;

    vertices[0] = -0.01;
    vertices[1] = -0.01;
    vertices[2] = -0.1;
    vertices[3] = vertices[12] = 0.01;
    vertices[4] = vertices[13] = -0.01;
    vertices[5] = vertices[14] = -0.1;
    vertices[6] = vertices[9] = -0.01;
    vertices[7] = vertices[10] = 0.01;
    vertices[8] = vertices[11] = -0.1;
    vertices[15] = 0.01;
    vertices[16] = 0.01;
    vertices[17] = -0.1;
    mesh.numVertices = 6;
    mesh.numIndices = 0;
    mesh.vertices = vertices;
    mesh.indices = NULL;
    mesh.flags = 0;

    if ((ret = malloc(sizeof(*ret) + sizeof(*mparams) + sizeof(*geom)))) {
        mparams = (void*)(ret + 1);
        geom = (void*)(mparams + 1);
        node_init(ret);

        solid_material_params_init(mparams);
        material_param_set_vec3_elems(&mparams->color, r, g, b);
        geom->vertexArray = vertex_array_new(&mesh);
        geom->material = solid_overlay_material_new(mesh.flags, mparams);
        if (!geom->vertexArray || !geom->material || !node_add_child(minimap, ret)) {
            minimap_point_free(ret);
            ret = NULL;
        }
        node_set_geometry(ret, geom);
        ret->alwaysDraw = 1;
    }

    return ret;
}

static void minimap_point_free(struct Node* point) {
    if (point) {
        struct SolidMaterialParams* mparams = (void*)(point + 1);
        struct Geometry* geom = (void*)(mparams + 1);
        vertex_array_free(geom->vertexArray);
        free(geom->material);
        free(point);
    }
}

void minimap_move_point(const struct Map* map, struct Node* point, float x, float z) {
    Vec3 t;
    t[0] = z / (map->size * map->unitSize * 5.0);
    t[1] = x / (map->size * map->unitSize * 5.0);
    t[2] = 0.0;
    node_set_pos(point, t);
}
