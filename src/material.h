#include <3dmr/skybox.h>
#include <3dmr/material/phong.h>
#include <3dmr/render/vertex_shader.h>

#ifndef MATERIAL_H
#define MATERIAL_H

void material_enable_fog(void);

GLuint fog_phong_shader_new(const struct PhongMaterialParams* params);

struct Material* sl3d_material_new(enum MeshFlags mflags, const struct PhongMaterialParams* params);

int fog_skybox_create(struct Skybox* skybox);
struct Skybox* fog_skybox_new(void);

#endif
