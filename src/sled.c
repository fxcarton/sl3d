#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <float.h>
#include <3dmr/material/phong.h>
#include <3dmr/math/linear_algebra.h>
#include <3dmr/mesh/obj.h>
#include <3dmr/render/texture.h>

#include "sled.h"
#include "map.h"

#define SKI_SUBDIVISIONS 16

#define GRAVITY 5.0
#define K1 2.0
#define K2 5.0

#define SKI_BACK_X -1.42
#define SKI_FRONT_X 1.14
#define SKI_Y 0.40
#define MASS 50.0
#define INERTIA 10.0
#define SKI_LENGTH_X 2.96
#define SKI_LENGTH_Y 0.82
#define SKI_LENGTH_Z 1.01

struct Sled* sled_new(void) {
    struct Mesh mesh;
    struct Sled* sled;
    struct Material* material;
    struct VertexArray* vertexArray = NULL;
    GLuint texture;

    if ((sled = malloc(sizeof(struct Sled)))) {
        if (make_obj(&mesh, SL3D_DATA_PATH "/models/sled.obj", 0, 1, 1)) {
            vertexArray = vertex_array_new(&mesh);
            mesh_free(&mesh);
        }
        if ((texture = texture_load_from_png(SL3D_DATA_PATH "/textures/sled.png"))) {
            phong_material_params_init(&sled->phongParams);
            material_param_set_vec3_texture(&sled->phongParams.ambient, texture);
            material_param_set_vec3_texture(&sled->phongParams.diffuse, texture);
            material_param_set_vec3_texture(&sled->phongParams.specular, texture);
            material_param_set_float_constant(&sled->phongParams.shininess, 1.0);
        }
        if (vertexArray && texture) {
            if ((material = phong_material_new(vertexArray->flags, &sled->phongParams))) {
                node_init(&sled->node);
                sled->geometry.material = material;
                sled->geometry.vertexArray = vertexArray;
                node_set_geometry(&sled->node, &sled->geometry);
                zero3v(sled->speed);
                quaternion_load_id(sled->angularSpeed);
                zero3v(sled->traction);
                sled->brake = 0.01;
                sled->isFlying = 0;
                sled->isGliding = 0;
                return sled;
            }
        }
        if (texture) glDeleteTextures(1, &texture);
        vertex_array_free(vertexArray);
        free(sled);
    }

    return NULL;
}

void sled_free(struct Sled* sled) {
    if (sled) {
        vertex_array_free(sled->geometry.vertexArray);
        glDeleteTextures(1, &sled->phongParams.ambient.value.texture);
        free(sled->geometry.material);
        free(sled);
    }
}

static int check_contacts(struct Node* node, struct Map* map, float contacts[2][SKI_SUBDIVISIONS], int contactState[2][2]) {
    int i, j;
    Vec3 v = {0, 0, 0}, sledPoint;
    float d, dMin;

    for (j = 0; j < 2; j++) {
        contactState[j][0] = -1;
        contactState[j][1] = -1;
        dMin = 0.0;
        for (i = 0; i < SKI_SUBDIVISIONS; i++) {
            if (i == SKI_SUBDIVISIONS / 2) dMin = 0.0;
            v[0] = SKI_BACK_X + ((float)i) * (SKI_FRONT_X - SKI_BACK_X) / (SKI_SUBDIVISIONS - 1.0);
            v[2] = ((float)(2 * j - 1)) * SKI_Y;
            quaternion_compose(sledPoint, node->orientation, v);
            incr3v(sledPoint, node->position);
            if ((d = map_get_height(map, sledPoint[0], sledPoint[2])) == FLT_MAX) return 0;
            if ((d = sledPoint[1] - d) < -0.01) {
                if (d < dMin) {
                    contactState[j][i < SKI_SUBDIVISIONS / 2] = i;
                    dMin = d;
                }
            }
            contacts[j][i] = d;
        }
    }

    return 1;
}

static float overlap(struct Node* node, struct Map* map) {
    int i, j;
    Vec3 v = {0, 0, 0}, sledPoint;
    float d, dMin = FLT_MAX;

    for (j = 0; j < 2; j++) {
        for (i = 0; i < SKI_SUBDIVISIONS; i++) {
            v[0] = SKI_BACK_X + ((float)i) * (SKI_FRONT_X - SKI_BACK_X) / (SKI_SUBDIVISIONS - 1.0);
            v[2] = ((float)(2 * j - 1)) * SKI_Y;
            quaternion_compose(sledPoint, node->orientation, v);
            incr3v(sledPoint, node->position);
            if ((d = sledPoint[1] - map_get_height(map, sledPoint[0], sledPoint[2])) < dMin) {
                dMin = d;
            }
        }
    }

    return dMin;
}

static void compute_acceleration(struct Sled* sled, Vec3 acceleration, Quaternion qAcc, float dt) {
    Vec3 v, tmp;
    float angle;
    Quaternion r, s;

    memcpy(acceleration, sled->traction, sizeof(Vec3));

    mul3sv(tmp, (sled->isGliding ? K2 : K1) * norm3(sled->speed), sled->speed);
    decr3v(acceleration, tmp);

    scale3v(acceleration, 1.0 / MASS);
    acceleration[1] -= GRAVITY;

    scale3v(acceleration, 1.0 - sled->brake);
    mul3sv(tmp, sled->brake / dt, sled->speed);
    decr3v(acceleration, tmp);

    quaternion_compose(v, sled->node.orientation, VEC3_AXIS_X);
    compute_rotation(v, sled->traction, tmp, &angle);
    quaternion_set_axis_angle(r, tmp, angle);
    quaternion_decompose_swing_twist(r, VEC3_AXIS_Y, s, qAcc);
}

static void get_vecs(struct Node* node, int i, int j, float d, Vec3 u, Vec3 v) {
    Vec3 t;
    t[0] = SKI_BACK_X + ((float)i) * (SKI_FRONT_X - SKI_BACK_X) / (SKI_SUBDIVISIONS - 1.0);
    t[2] = ((float)(2 * j - 1)) * SKI_Y;
    t[1] = 0;
    quaternion_compose(u, node->orientation, t);
    v[0] = u[0];
    v[1] = u[1] - d;
    v[2] = u[2];
}

static void get_vec_middle_x(struct Node* node, int i, Vec3 u) {
    Vec3 t;
    t[0] = SKI_BACK_X + ((float)i) * (SKI_FRONT_X - SKI_BACK_X) / (SKI_SUBDIVISIONS - 1.0);
    t[1] = t[2] = 0;
    quaternion_compose(u, node->orientation, t);
}

static void get_vec_middle_z(struct Node* node, int j, Vec3 u) {
    Vec3 t;
    t[2] = ((float)(2 * j - 1)) * SKI_Y;
    t[0] = t[1] = 0;
    quaternion_compose(u, node->orientation, t);
}

static void sled_rotate_1(struct Node* node, Vec3 u, Vec3 v, Quaternion angularSpeed, float dt) {
    Vec3 axis;
    float angle;
    compute_rotation(u, v, axis, &angle);
    node_rotate(node, axis, angle);
    quaternion_set_axis_angle(angularSpeed, axis, angle / (dt * INERTIA));
}

static void sled_rotate_2(struct Node* node, Vec3 o1, Vec3 u1, Vec3 v1, Vec3 o2, Vec3 u2, Vec3 v2, Quaternion angularSpeed, float dt) {
    Vec3 u, v, axis1, axis2;
    float angle1, angle2;

    sub3v(u, u1, o1);
    sub3v(v, v1, o1);
    compute_rotation(u, v, axis1, &angle1);

    sub3v(u, u2, o2);
    sub3v(v, v2, o2);
    compute_rotation(u, v, axis2, &angle2);

    if (angle1 < angle2) {
        node_rotate(node, axis2, angle2);
        quaternion_set_axis_angle(angularSpeed, axis2, angle2 / (dt * INERTIA));
    } else {
        node_rotate(node, axis1, angle1);
        quaternion_set_axis_angle(angularSpeed, axis1, angle1 / (dt * INERTIA));
    }
}

static void sled_rotate_4(struct Node* node, int contactState[2][2], float contacts[2][SKI_SUBDIVISIONS], Quaternion angularSpeed, float dt) {
    Vec3 u00, v00, u01, v01, u10, v10, u11, v11;
    Vec3 u, v, axis1, axis2;
    float angle1, angle2;

    get_vecs(node, contactState[0][0], 0, contacts[0][contactState[0][0]], u00, v00);
    get_vecs(node, contactState[0][1], 0, contacts[0][contactState[0][1]], u01, v01);
    get_vecs(node, contactState[1][0], 1, contacts[1][contactState[1][0]], u10, v10);
    get_vecs(node, contactState[1][1], 1, contacts[1][contactState[1][1]], u11, v11);

    sub3v(u, u00, u01);
    sub3v(v, v00, v01);
    compute_rotation(u, v, axis1, &angle1);

    sub3v(u, u10, u11);
    sub3v(v, v10, v11);
    compute_rotation(u, v, axis2, &angle2);

    if (angle1 < angle2) {
        node_rotate(node, axis2, angle2);
        quaternion_set_axis_angle(angularSpeed, axis2, angle2 / (dt * INERTIA));
        u[0] = 0;
        u[1] = -(contacts[1][contactState[1][0]] + contacts[1][contactState[1][1]]) / 2.0;
        u[2] = 0;
        node_translate(node, u);
    } else {
        node_rotate(node, axis1, angle1);
        quaternion_set_axis_angle(angularSpeed, axis1, angle1 / (dt * INERTIA));
        u[0] = 0;
        u[1] = -(contacts[0][contactState[0][0]] + contacts[0][contactState[0][1]]) / 2.0;
        u[2] = 0;
        node_translate(node, u);
    }

    /* TODO: rotation around the sled'x-axis */
}

enum SledUpdateError sled_update(struct Sled* sled, struct Map* map, float dt) {
    float dist;
    int contactState[2][2];
    int i, j, c00, c01, c10, c11, isFlying;
    Vec3 acceleration, t, u, v, w, w1, w2;
    Quaternion qAcc, qS;
    float contacts[2][SKI_SUBDIVISIONS];

    /* Compute the acceleration
     ***************************/
    compute_acceleration(sled, acceleration, qAcc, dt);
    if (sled->isGliding) {
        quaternion_compose(v, sled->node.orientation, VEC3_AXIS_X);
        mul3sv(acceleration, dot3(acceleration, v), v);
        mul3sv(sled->speed, dot3(sled->speed, v), v);
    }

    /* Update speed = integrate acceleration
     ****************************************/
    /* v(t + dt) = v(t) + a dt */
    scale3v(acceleration, dt);
    incr3v(sled->speed, acceleration);
    /* Rotation around world up */
    memcpy(qS, sled->angularSpeed, sizeof(Quaternion));
    quaternion_mul(sled->angularSpeed, qAcc, qS);

    /* Update model = integrate speeds
     **********************************/
    mul3sv(t, dt, sled->speed);
    node_translate(&sled->node, t);

    quaternion_get_axis(sled->angularSpeed, t);
    node_rotate(&sled->node, t, quaternion_get_angle(sled->angularSpeed) * dt * (1.0 - sled->brake));

    /* Check collisions
     *******************/
    if (!check_contacts(&sled->node, map, contacts, contactState)) return SLED_OUT_OF_MAP;
    c00 = contactState[0][0] >= 0;
    c01 = contactState[0][1] >= 0;
    c10 = contactState[1][0] >= 0;
    c11 = contactState[1][1] >= 0;
    sled->isGliding = ((c00 && c01) || (c10 && c11));
    isFlying = 0;
    i = j = 0;
    switch (c00 + c01 + c10 + c11) {
        case 0:
            isFlying = 1;
            break;
        case 1:
            j = c10 + c11;
            i = contactState[j][c01 + c11];
            get_vecs(&sled->node, i, j, contacts[j][i], u, v);
            sled_rotate_1(&sled->node, u, v, sled->angularSpeed, dt);
            break;
        case 2:
            if ((c00 && c01) || (j = (c10 && c11))) {
                get_vecs(&sled->node, contactState[j][0], j, contacts[j][contactState[j][0]], u, w1);
                get_vecs(&sled->node, contactState[j][1], j, contacts[j][contactState[j][1]], v, w2);
                get_vec_middle_x(&sled->node, 0, t);
                get_vec_middle_x(&sled->node, 1, w);
                sled_rotate_2(&sled->node, t, u, w1, w, v, w2, sled->angularSpeed, dt);
            } else if ((c00 && c10) || (i = (c01 && c11))) {
                get_vecs(&sled->node, contactState[0][i], 0, contacts[0][contactState[0][i]], u, w1);
                get_vecs(&sled->node, contactState[1][i], 1, contacts[1][contactState[1][i]], v, w2);
                get_vec_middle_z(&sled->node, 0, t);
                get_vec_middle_z(&sled->node, 1, w);
                sled_rotate_2(&sled->node, t, u, w1, w, v, w2, sled->angularSpeed, dt);
            } else {
                get_vecs(&sled->node, contactState[0][c01], 0, contacts[0][contactState[0][c01]], u, w1);
                get_vecs(&sled->node, contactState[1][!c01], 0, contacts[1][contactState[1][!c01]], v, w2);
                decr3v(u, v);
                decr3v(w1, w2);
                sled_rotate_1(&sled->node, u, w1, sled->angularSpeed, dt);
            }
            break;
        case 3:
            if (!c00) contacts[0][(contactState[0][0] = (SKI_SUBDIVISIONS - 1))] = 0;
            if (!c01) contacts[0][(contactState[0][1] = 0)] = 0;
            if (!c10) contacts[1][(contactState[1][0] = (SKI_SUBDIVISIONS - 1))] = 0;
            if (!c11) contacts[1][(contactState[1][1] = 0)] = 0;
        case 4:
            sled_rotate_4(&sled->node, contactState, contacts, sled->angularSpeed, dt);
            break;
    }

    if ((dist = overlap(&sled->node, map)) < 0.0) {
        t[0] = t[2] = 0; t[1] = -dist - 0.01 * sled->isGliding;
        node_translate(&sled->node, t);
    }

    if (sled->isFlying && !isFlying) {
        quaternion_load_id(sled->angularSpeed);
    }
    sled->isFlying = isFlying;

    if (quaternion_get_angle(sled->angularSpeed) > M_PI / 2.0) {
        quaternion_set_angle(sled->angularSpeed, M_PI / 2.0 - quaternion_get_angle(sled->angularSpeed));
        quaternion_load_id(sled->angularSpeed);
    }

    return SLED_NO_ERROR;
}
