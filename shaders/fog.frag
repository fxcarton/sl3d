#version 140
#include <phong/frag.glsl>

in float pixDistance;

vec4 compute_fog(vec4 color, float distance) {
    vec4 fogColor = vec4(0.7, 0.7, 0.8, 1.0);
    float opaqueDistance = 200;

    if (distance > opaqueDistance) {
        return fogColor;
    } else {
        float u = distance/opaqueDistance;
        return vec4((1 - u) * vec3(color) + u * vec3(fogColor), color[3]);
    }
}

void main() {
    phong_frag_main();
    out_Color = compute_fog(out_Color, pixDistance);
}
