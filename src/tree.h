#include <3dmr/scene/node.h>

#ifndef TREE_H
#define TREE_H

struct Geometry* tree_geometry(void);
void tree_geometry_free(struct Geometry* geom);

struct Geometry* tree_reduced_geometry(const struct Geometry* geom);
void tree_reduced_geometry_free(struct Geometry* geom);

#endif
