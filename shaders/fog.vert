#version 140
#include <standard.glsl>

out float pixDistance;

void main() {
    vec3 pixPos = vec3(model * vec4(in_Vertex, 1.0));
    pixDistance = distance(cameraPosition, pixPos);

    standard_vert_main();
}
