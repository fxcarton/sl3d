CFLAGS ?= -std=c89 -pedantic -march=native -Wall -D_XOPEN_SOURCE=500 -D_POSIX_C_SOURCE=200112L -O3
PREFIX ?= $(HOME)/.local
BINDIR ?= bin
DATADIR ?= share/SL3D
DIST ?=

DEPS := '3dmr >= 0.1-126'
APP := SL3D
SOURCES := $(wildcard src/*.c)
OBJECTS := $(SOURCES:.c=.o)
CFLAGS += $(shell pkg-config --cflags $(DEPS)) -DSL3D_DATA_PATH=\"$(if $(DIST),$(PREFIX)/$(DATADIR),.)\"
LDLIBS += -lm $(shell pkg-config --libs-only-l $(DEPS))
LDFLAGS += $(shell pkg-config --libs-only-L --libs-only-other $(DEPS))

.PHONY: all
all: $(APP)
$(APP): $(OBJECTS)
	$(CC) $(LDFLAGS) -o $@ $^ $(LDLIBS)
$(APP) $(OBJECTS): | check_deps

.PHONY: check_deps
check_deps:
	@pkg-config --exists $(DEPS) || (echo "One of the following dependencies was not found: $(DEPS)"; false)

.PHONY: clean
clean:
	rm -f $(wildcard $(APP) $(OBJECTS))

.PHONY: install
D := $(if $(DESTDIR),$(DESTDIR)/)$(PREFIX)
install: all
	mkdir -p $(D)/$(BINDIR) $(D)/$(DATADIR)
	cp SL3D $(D)/$(BINDIR)
	cp -R models shaders textures $(D)/$(DATADIR)
