#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <unistd.h>
#include <3dmr/light/light.h>
#include <3dmr/render/camera_buffer_object.h>
#include <3dmr/render/lights_buffer_object.h>
#include <3dmr/math/linear_algebra.h>
#include <3dmr/scene/node.h>
#include <3dmr/light/directional_light.h>

#include "game.h"
#include "material.h"
#include "minimap.h"
#include "forest.h"
#include "tree.h"
#include "flag.h"

#define NEAR_TO_FAR 50
#define FAR_TO_INF 250

static void cursor_callback(struct Viewer* viewer, double xpos, double ypos, double dx, double dy, int buttonLeft, int buttonMiddle, int buttonRight, void* userData) {
    struct Game* game = userData;
    struct Camera* camera = &game->camera;
    Vec4 u = {0, 0, 0, 1}, w = {0, 1, 0, 0};
    Quaternion a, b, c, orientation;
    Vec3 position;

    mul4mv(u, MAT_CONST_CAST(game->sled->node.transform), w);
    quaternion_set_axis_angle(a, u, -dx / game->viewer->width);
    normalize4(a);
    camera_get_right(MAT_CONST_CAST(camera->view), u);
    camera_get_position(MAT_CONST_CAST(camera->view), position);
    camera_get_orientation(MAT_CONST_CAST(camera->view), orientation);
    normalize4(orientation);
    quaternion_set_axis_angle(b, u, -dy / game->viewer->height);
    normalize4(b);
    quaternion_mul(c, a, orientation);
    quaternion_mul(orientation, b, c);

    camera_view(position, orientation, camera->view);
}

static void key_callback(struct Viewer* viewer, int key, int scancode, int action, int mods, void* userData) {
    struct Game* game = userData;
    int i;

    if (action == GLFW_RELEASE && key == GLFW_KEY_SPACE) {
        game->sled->brake = 0.01;
    }

    if (action != GLFW_PRESS && action != GLFW_REPEAT) {
        return;
    }

    switch (key) {
        case GLFW_KEY_ESCAPE:
            game->running = 0;
            break;
        case GLFW_KEY_LEFT:
        case GLFW_KEY_A:
            dog_rotate(game->dog[NB_DOGS - 1], 0.1);
            break;
        case GLFW_KEY_RIGHT:
        case GLFW_KEY_D:
            dog_rotate(game->dog[NB_DOGS - 1], -0.1);
            break;
        case GLFW_KEY_DOWN:
        case GLFW_KEY_S:
            for (i = 0; i < NB_DOGS; i++) {
                dog_decr_force(game->dog[i]);
            }
            break;
        case GLFW_KEY_UP:
        case GLFW_KEY_W:
            for (i = 0; i < NB_DOGS; i++) {
                dog_incr_force(game->dog[i]);
            }
            break;
        case GLFW_KEY_SPACE:
            game->sled->brake = 0.2;
            break;
    }
}

static void resize_callback(struct Viewer* viewer, void* userData) {
    struct Game* game = userData;
    glViewport(0, 0, viewer->width, viewer->height);
    camera_set_ratio((double)viewer->width / (double)viewer->height, game->camera.projection);
    camera_buffer_object_update_projection(&game->scene.camera, MAT_CONST_CAST(game->camera.projection));
}

static void close_callback(struct Viewer* viewer, void* userData) {
    struct Game* game = userData;
    game->running = 0;
}

int game_init(struct Game* game, const struct Params* params) {
    Quaternion q;
    Vec3 t;
    unsigned int i;
    enum PullType pullType = PULL_SLED;
    void* previous;
    struct Node* mapFlagPoint;

    game->running = 0;
    if (!(game->viewer = viewer_new(1024, 768, "SL3D"))) return 0;
    viewer_set_cursor_mode(game->viewer, VIEWER_CURSOR_DISABLED);
    game->viewer->cursor_callback = cursor_callback;
    game->viewer->key_callback = key_callback;
    game->viewer->resize_callback= resize_callback;
    game->viewer->close_callback = close_callback;
    game->viewer->callbackData = game;

    if (!scene_init(&game->scene, NULL)) {
        viewer_free(game->viewer);
        return 0;
    }
    {
        struct DirectionalLight sun = {{1, -1, -1}, {0.5, 0.5, 0.5}};
        lights_buffer_object_update_dlight(&game->scene.lights, &sun, 0);
        lights_buffer_object_update_ndlight(&game->scene.lights, 1);
        uniform_buffer_send(&game->scene.lights);
    }
    camera_projection(1024.0 / 768.0, 1.04, 0.1, 2000, game->camera.projection);
    camera_buffer_object_update_projection(&game->scene.camera, MAT_CONST_CAST(game->camera.projection));

    game->skybox = NULL;
    game->skyboxTex = 0;
    game->map = NULL;
    game->forest = NULL;
    game->fullTree = NULL;
    game->reducedTree = NULL;
    game->flag = NULL;
    game->sled = NULL;
    for (i = 0; i < NB_DOGS; i++) {
        game->dog[i] = NULL;
    }
    game->minimap = NULL;
    game->mapSledPoint = NULL;

    if (params->fog) {
        material_enable_fog();
        if (!(game->skybox = fog_skybox_new())) goto error;
    } else {
        if (!(game->skyboxTex = skybox_load_texture_png_6faces(
                        SL3D_DATA_PATH "/textures/skybox_0.png",
                        SL3D_DATA_PATH "/textures/skybox_2.png",
                        SL3D_DATA_PATH "/textures/skybox_5.png",
                        SL3D_DATA_PATH "/textures/skybox_4.png",
                        SL3D_DATA_PATH "/textures/skybox_3.png",
                        SL3D_DATA_PATH "/textures/skybox_1.png"))
         || !(game->skybox = skybox_new(game->skyboxTex))) goto error;
    }

    if (!(game->map = map_new(1024, 16, 1.5, params->terrainRoughness))
     || !(game->forest = forest_generate(game->map, params->forestDensity, params->forestCompactness, 64, &game->numTrees))
     || !scene_add(&game->scene, game->map->master)
     || !(game->flag = flag_new(game->map))
     || !scene_add(&game->scene, game->flag)
     || !(game->minimap = minimap_new(game->map->height, game->map->size))
     || !scene_add(&game->scene, game->minimap)
     || !(game->mapSledPoint = minimap_add_point(game->minimap, 0.4, 0.2, 0.0))
     || !(mapFlagPoint = minimap_add_point(game->minimap, 0.43, 0.63, 0.14))) goto error;

    minimap_move_point(game->map, mapFlagPoint, game->flag->position[0], game->flag->position[2]);

    if (!(game->fullTree = tree_geometry())
     || !(game->reducedTree = tree_reduced_geometry(game->fullTree))) goto error;

    {
        struct Node *tree, *lasttree = game->forest + game->numTrees;
        for (tree = game->forest; tree < lasttree; tree++) {
            node_set_geometry(tree, game->fullTree);
        }
    }

    if (!(previous = game->sled = sled_new())) goto error;
    t[0] = 256.0 * 1.5 / 2.0;
    t[2] = 256.0 * 1.5 / 2.0;
    t[1] = map_get_height(game->map, t[0], t[2]);
    node_translate(&game->sled->node, t);
    if (!scene_add(&game->scene, &game->sled->node)) goto error;

    for (i = 0; i < NB_DOGS; i++) {
        if (!(previous = game->dog[i] = dog_new(pullType, previous))) goto error;
        pullType = PULL_DOG;
        t[0] += 3;
        t[1] = map_get_height(game->map, t[0], t[2]);
        node_translate(game->dog[i]->nodes, t);
        if (!scene_add(&game->scene, game->dog[i]->nodes)) goto error;
    }

    t[0] = 256.0 * 1.5 / 2.0 - 3.0;
    t[1] = map_get_height(game->map, t[0], t[2]);
    q[0] = 0.707107;
    q[1] = q[3] = 0;
    q[2] = -q[0];
    camera_view(t, q, game->camera.view);

    scene_update_nodes(&game->scene, NULL, NULL);

    return 1;

error:
    game_free(game);
    return 0;
}

void game_free(struct Game* game) {
    unsigned int i;

    game->scene.root.nbChildren = 0;
    viewer_free(game->viewer);
    scene_free(&game->scene, NULL);
    skybox_free(game->skybox);
    if (game->skyboxTex) glDeleteTextures(1, &game->skyboxTex);
    map_free(game->map);
    free(game->forest);
    tree_geometry_free(game->fullTree);
    tree_reduced_geometry_free(game->reducedTree);
    flag_free(game->flag);
    sled_free(game->sled);
    for (i = 0; i < NB_DOGS; i++) {
        dog_free(game->dog[i]);
    }
    minimap_free(game->minimap);
}

void game_over(struct Game* game, const char* message) {
    printf("\x1b[31;1mGame over:\x1b[0;31m %s\x1b[0m\n", message); 
    game->running = 0;
}

void game_update(struct Game* game) {
    Quaternion rot, swing, twist, camorient, camoldorient;
    Vec4 u = {-2, 1.75, 0, 1}, v, x = {1, 0, 0, 0}, y = {0, 1, 0, 0}, up, axis;
    Vec3 campos;
    struct Camera* camera = &game->camera;
    double dt;
    float angle;
    
    viewer_process_events(game->viewer);
    dt = viewer_next_frame(game->viewer);

    switch (dog_update(game->dog[NB_DOGS - 1], game->map, dt)) {
        case DOG_NO_ERROR:
            break;
        case DOG_SLED_OUT_OF_MAP:
            game_over(game, "you sled got lost in the universe, and you as well, and you died.");
            break;
        case DOG_SLED_TOO_MUCH_ROTATION:
            game_over(game, "your sled rotated so much that you fell and died.");
            break;
        case DOG_SLED_COLLISION_WITH_TREE:
            game_over(game, "your sled crashed onto a tree, and you died.");
            break;
        case DOG_OUT_OF_MAP:
            game_over(game, "a dog got lost in the universe, the sled and you as well, and you died.");
            break;
        case DOG_COLLISION_WITH_TREE:
            game_over(game, "a dog crashed onto a tree, the sled and you as well, and you died.");
            break;
        case DOG_COLLISION_WITH_SLED:
            game_over(game, "your sled killed a dog, so the others attacked you and you died.");
            break;
    }

    scene_update_nodes(&game->scene, NULL, NULL);

    mul4mv(v, MAT_CONST_CAST(game->sled->node.model), u);
    memcpy(campos, v, sizeof(Vec3));
    mul4mv(v, MAT_CONST_CAST(game->sled->node.model), y);
    camera_get_up(MAT_CONST_CAST(camera->view), up);
    compute_rotation(up, v, axis, &angle);
    quaternion_set_axis_angle(rot, axis, angle);
    mul4mv(v, MAT_CONST_CAST(game->sled->node.model), x);
    quaternion_decompose_swing_twist(rot, v, swing, twist);
    camera_get_orientation(MAT_CONST_CAST(game->camera.view), camoldorient);
    quaternion_mul(camorient, twist, camoldorient);
    camera_view(campos, camorient, camera->view);
    camera_buffer_object_update_view(&game->scene.camera, MAT_CONST_CAST(game->camera.view));
    camera_buffer_object_update_position(&game->scene.camera, campos);
    uniform_buffer_send(&game->scene.camera);

    minimap_move_point(game->map, game->mapSledPoint, campos[0], campos[2]);
    
    { /* XXX: this is a rough translation of what the original Renderable LOD code did.
         This is not efficient since it goes through every tree.
         It could be improved to only look to trees that are on map cells that are adjacent to the camera.
         Since it's very easy to access cells at a given position, it should be easy to only look at cells
         that potentially entered/left a NEAR/FAR/INF area.
         */
        Vec3 d;
        float dist;
        struct Geometry* geoms[2];
        struct Node *tree, *lasttree = game->forest + game->numTrees;
        geoms[0] = game->fullTree;
        geoms[1] = game->reducedTree;
        for (tree = game->forest; tree < lasttree; tree++) {
            sub3v(d, tree->transform[3], campos);
            dist = norm3sq(d);
            if (!(tree->alwaysDraw = -(dist >= FAR_TO_INF * FAR_TO_INF))) {
                tree->data.geometry = geoms[dist >= NEAR_TO_FAR * NEAR_TO_FAR];
            }
        }
    }

    scene_update_render_queue(&game->scene, MAT_CONST_CAST(game->camera.view), MAT_CONST_CAST(game->camera.projection));
    scene_render(&game->scene);
    if (game->skybox) skybox_render(game->skybox);
}
