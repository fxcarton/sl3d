#include "map.h"

#ifndef FOREST_H
#define FOREST_H

struct Node* forest_generate(struct Map* map, float density, float compactness, int forestSize, size_t* numTrees);

#endif
