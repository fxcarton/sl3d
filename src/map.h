#include <3dmr/material/phong.h>
#include <3dmr/scene/node.h>

#ifndef MAP_H
#define MAP_H

struct Map {
    unsigned int size, nbCellSize;
    float unitSize;
    float* height;

    struct Node* master;
    struct PhongMaterialParams phongParams;
};

struct Map* map_new(unsigned int size, unsigned int cellSize, float unitSize, float roughness);
void map_free(struct Map* map);

float map_get_height(const struct Map* map, float x, float y);
int map_get_normal(const struct Map* map, float x, float y, Vec3 normal);

#endif
