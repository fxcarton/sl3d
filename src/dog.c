#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <float.h>
#include <3dmr/animation/animation.h>
#include <3dmr/animation/play.h>
#include <3dmr/material/phong.h>
#include <3dmr/math/linear_algebra.h>
#include <3dmr/math/quaternion.h>
#include <3dmr/mesh/obj.h>
#include <3dmr/render/texture.h>
#include <3dmr/scene/node.h>

#include "dog.h"
#include "sled.h"
#include "map.h"

#define ROPE_SIZE 4.0

static void keyframe(struct Animation* anim, unsigned int i, float t, float v) {
    anim->tracks[TRACK_Z_ROT].times.values.linear[i] = t;
    anim->tracks[TRACK_Z_ROT].values.values.linear[i] = v;
}

#define FRAMES_PER_SEGMENT 16U
static void keyframes(struct Animation* anim, float t1, float v1, float t2, float v2) {
    float dt, dv;
    unsigned int i, n0 = ((float)FRAMES_PER_SEGMENT) * t1 / (1.0f - (t2 - t1)), n1 = n0 + FRAMES_PER_SEGMENT;
    for (i = 0; i < n0; i++) {
        dt = (float)(i + FRAMES_PER_SEGMENT - n0) / ((float)FRAMES_PER_SEGMENT);
        dv = (cos((1 - dt) * M_PI) + 1.0) / 2.0;
        keyframe(anim, i, t2 + (1.0f - (t2 - t1)) * dt - 1.0f, v2 + (v1 - v2) * dv);
    }
    for (; i < n1; i++) {
        dt = (float)(i - n0) / ((float)FRAMES_PER_SEGMENT);
        dv = (cos((1 - dt) * M_PI) + 1.0) / 2.0;
        keyframe(anim, i, t1 + (t2 - t1) * dt, v1 + (v2 - v1) * dv);
    }
    for (; i < 2 * FRAMES_PER_SEGMENT; i++) {
        dt = (float)(i - n1) / ((float)FRAMES_PER_SEGMENT);
        dv = (cos((1 - dt) * M_PI) + 1.0) / 2.0;
        keyframe(anim, i, t2 + (1.0f - (t2 - t1)) * dt, v2 + (v1 - v2) * dv);
    }
}

#define MODEL_PATH(n) SL3D_DATA_PATH "/models/" n ".obj"
struct Dog* dog_new(enum PullType pullType, void* pulledObject) {
    Vec3 offset[2] = {{0.205, 0.413, 0}, {-0.257, 0.437, 0}};
    const char* objPath[] = {MODEL_PATH("dog-trunk"), MODEL_PATH("dog-f-l-paw"), MODEL_PATH("dog-f-r-paw"), MODEL_PATH("dog-b-l-paw"), MODEL_PATH("dog-b-r-paw")};
    struct Mesh mesh;
    struct Dog* dog;
    unsigned int i, j;
    GLuint texture;
    int ok = 1;

    if (!(dog = malloc(sizeof(*dog)))) {
        return NULL;
    }

    node_init(dog->nodes);
    for (j = 0; j < 5; j++) {
        node_init(dog->nodes + j + 1);
        for (i = 0; i < DOG_NB_STATUS; i++) {
            anim_animation_zero(dog->anims[i] + j);
        }
        dog->geoms[j].vertexArray = NULL;
        dog->geoms[j].material = NULL;
    }
    dog->phongParams.ambient.value.texture = 0;

    for (j = 0; ok && j < 5; j++) {
        for (i = 0; ok && i < DOG_NB_STATUS; i++) {
            ok = anim_animation_init(dog->anims[i] + j, dog->nodes + j + 1)
                && anim_track_init(dog->anims[i][j].tracks + TRACK_Z_ROT, TRACK_LINEAR, TRACK_LINEAR, (i == DOG_IDLE) ? 2U : (2U * FRAMES_PER_SEGMENT));
        }
    }

    if (ok) {
        for (j = 0; j < 5; j++) {
            for (i = 0; i < DOG_NB_STATUS; i++) {
                dog->anims[i][j].flags = TRACKING_ROT;
            }
            keyframe(dog->anims[DOG_IDLE] + j, 0, 0, 0);
            keyframe(dog->anims[DOG_IDLE] + j, 1, 0, 0);
        }
        keyframes(dog->anims[DOG_WALKING], 0, -0.02, 0.5, 0.02);
        keyframes(dog->anims[DOG_RUNNING], 0, -0.1, 0.5, 0.1);
        keyframes(dog->anims[DOG_WALKING] + 1, 0, -0.3, 0.5, 0.3);
        keyframes(dog->anims[DOG_RUNNING] + 1, 0, -0.7, 0.5, 0.8);
        keyframes(dog->anims[DOG_WALKING] + 2, 0.5, -0.3, 1, 0.3);
        keyframes(dog->anims[DOG_RUNNING] + 2, 0.2, -0.7, 0.7, 0.8);
        keyframes(dog->anims[DOG_WALKING] + 3, 0.25, -0.3, 0.75, 0.3);
        keyframes(dog->anims[DOG_RUNNING] + 3, 0, 0.83, 0.5, -0.7);
        keyframes(dog->anims[DOG_WALKING] + 4, 0.25, 0.3, 0.75, -0.3);
        keyframes(dog->anims[DOG_RUNNING] + 4, 0.18, 0.8, 0.58, -0.75);
    }

    for (j = 0; ok && j < 5; j++) {
        if (make_obj(&mesh, objPath[j], 0, 1, 1)) {
            dog->geoms[j].vertexArray = vertex_array_new(&mesh);
            mesh_free(&mesh);
        }
        ok = dog->geoms[j].vertexArray != NULL;
    }

    if ((texture = texture_load_from_png(SL3D_DATA_PATH "/textures/dog.png"))) {
        phong_material_params_init(&dog->phongParams);
        material_param_set_vec3_texture(&dog->phongParams.ambient, texture);
        material_param_set_vec3_texture(&dog->phongParams.diffuse, texture);
        material_param_set_vec3_texture(&dog->phongParams.specular, texture);
        material_param_set_float_constant(&dog->phongParams.shininess, 1.0);
    } else {
        ok = 0;
    }

    ok = ok && (dog->geoms[0].material = phong_material_new(dog->geoms[0].vertexArray->flags, &dog->phongParams))
            && node_add_child(dog->nodes, dog->nodes + 1);

    for (j = 1; ok && j < 5; j++) {
        dog->geoms[j].material = dog->geoms[0].material;
        node_set_geometry(dog->nodes + j + 1, dog->geoms + j);
        node_translate(dog->nodes + j + 1, offset[(j - 1) >> 1]);
        ok = node_add_child(dog->nodes + 1, dog->nodes + j + 1);
    }

    if (!ok) {
        dog_free(dog);
        return NULL;
    }

    node_set_geometry(dog->nodes + 1, dog->geoms);
    dog->status = DOG_IDLE;
    dog->forceNorm = 0;
    dog->animt = 0;
    zero3v(dog->traction);
    zero3v(dog->goal);
    dog->pullType = pullType;
    dog->pulledObject = pulledObject;

    return dog;
}

static void update_status(struct Dog* dog) {
    if (dog->forceNorm == 0) {
        dog->status = DOG_IDLE;
    } else if (dog->forceNorm < 2) {
        dog->status = DOG_WALKING;
    } else {
        dog->status = DOG_RUNNING;
    }
}

void dog_incr_force(struct Dog* dog) {
    if ((dog->forceNorm += 0.1) > 5.0) {
        dog->forceNorm = 5.0;
    }
    update_status(dog);
}

void dog_decr_force(struct Dog* dog) {
    if ((dog->forceNorm -= 0.1) < 0.0) {
        dog->forceNorm = 0.0;
    }
    update_status(dog);
}

void dog_rotate(struct Dog* dog, float angle) {
    node_rotate(dog->nodes, VEC3_AXIS_Y, angle);
}

enum DogUpdateError dog_update(struct Dog* dog, struct Map* map, float dt) {
    int i;
    float ps, angle, dist, dMin;
    Vec3 t, o, os, s, y, z, n;
    Quaternion rot, swing, twist;
    enum DogUpdateError error;
    const float speedFactor = 1.0; /* was = 2.3; */

    dog->animt = fmod(dog->animt + dt * speedFactor, 1);
    for (i = 0; i < 5; i++) {
        anim_play(dog->anims[dog->status] + i, dog->animt);
    }

    memcpy(o, dog->nodes->position, sizeof(Vec3));
    quaternion_compose(z, dog->nodes->orientation, VEC3_AXIS_Z);
    if (!map_get_normal(map, o[0], o[2], n)) return DOG_OUT_OF_MAP;
    cross3(s, n, z);
    scale3v(s, dt * ((n[1] < 0) ? dog->forceNorm : (n[1] * dog->forceNorm)));
    incr3v(s, dog->traction);
    node_translate(dog->nodes, s);

    quaternion_compose(t, dog->nodes->orientation, VEC3_AXIS_X);
    compute_rotation(t, dog->goal, o, &angle);
    quaternion_set_axis_angle(rot, o, angle);
    quaternion_decompose_swing_twist(rot, VEC3_AXIS_Y, swing, twist);
    node_rotate_q(dog->nodes, twist);

    memcpy(o, dog->nodes->position, sizeof(Vec3));
    s[0] = s[2] = 0;
    if ((s[1] = map_get_height(map, o[0], o[2]) - o[1]) == FLT_MAX) return DOG_OUT_OF_MAP;
    node_translate(dog->nodes, s);

    switch (dog->pullType) {
        case PULL_DOG:
            memcpy(os, ((struct Dog*)dog->pulledObject)->nodes->position, sizeof(Vec3));
            decr3v(o, os);
            if (norm3(o) > ROPE_SIZE) {
                memcpy(((struct Dog*)(dog->pulledObject))->traction, o, sizeof(Vec3));
            } else {
                zero3v(((struct Dog*)(dog->pulledObject))->traction);
            }
            memcpy(((struct Dog*)(dog->pulledObject))->goal, o, sizeof(Vec3));
            if ((error = dog_update(dog->pulledObject, map, dt)) != DOG_NO_ERROR) return error;
            dMin = 0.77;
            break;
        case PULL_SLED:
            memcpy(os, ((struct Sled*)dog->pulledObject)->node.position, sizeof(Vec3));
            decr3v(o, os);
            if (norm3(o) > ROPE_SIZE) {
                mul3sv(((struct Sled*)(dog->pulledObject))->traction, 1.0 / dt, o);
            } else {
                zero3v(((struct Sled*)(dog->pulledObject))->traction);
            }
            if ((error = sled_update(dog->pulledObject, map, dt)) != SLED_NO_ERROR) return error;
            dMin = 1.5;
            break;
        case PULL_NONE:
        default:
            zero3v(o);
            dMin = 0.0;
            break;
    }
    if ((dist = norm3(o)) > ROPE_SIZE) {
        scale3v(o, (ROPE_SIZE - dist) / dist);
        node_translate(dog->nodes, o);
    } else if (dist < dMin) {
        if (dog->pullType == PULL_SLED) {
            return DOG_COLLISION_WITH_SLED;
        } else {
            scale3v(o, (dMin - dist) / dist);
            node_translate(dog->nodes, o);
        }
    }

    memcpy(o, dog->nodes->position, sizeof(Vec3));
    if (!map_get_normal(map, o[0], o[2], n)) return DOG_OUT_OF_MAP;
    quaternion_compose(y, dog->nodes->orientation, VEC3_AXIS_Y);
    cross3(s, y, n);
    ps = dot3(y, n),
    angle = (ps > 1.0) ? 0.0 : ((ps < -1.0) ? M_PI : acos(ps));
    quaternion_set_axis_angle(rot, s, angle);
    quaternion_compose(z, dog->nodes->orientation, VEC3_AXIS_Z);
    quaternion_decompose_swing_twist(rot, z, swing, twist);
    node_rotate_q(dog->nodes, twist);
    s[0] = s[2] = 0;
    if ((s[1] = map_get_height(map, o[0], o[2]) - o[1]) == FLT_MAX) return DOG_OUT_OF_MAP;
    node_translate(dog->nodes, s);

    return DOG_NO_ERROR;
}

void dog_free(struct Dog* dog) {
    unsigned int i, j;

    if (dog) {
        if (dog->phongParams.ambient.value.texture) glDeleteTextures(1, &dog->phongParams.ambient.value.texture);
        free(dog->geoms[0].material);
        for (j = 0; j < 5; j++) {
            for (i = 0; i < DOG_NB_STATUS; i++) {
                anim_animation_free(dog->anims[i] + j);
            }
            vertex_array_free(dog->geoms[j].vertexArray);
        }
        nodes_free(dog->nodes, NULL);

        free(dog);
    }
}
