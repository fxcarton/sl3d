#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <float.h>
#include <3dmr/math/linear_algebra.h>
#include <3dmr/material/phong.h>
#include <3dmr/mesh/mesh.h>
#include <3dmr/render/texture.h>

#include "map.h"
#include "rand_tools.h"
#include "material.h"

static void set_cell_data(unsigned int x, unsigned int y, unsigned int mapSize, float unitSize, float* height, float* vertex, float* normal) {
    unsigned int center = y * mapSize + x;
    unsigned int yPrev = (y - 1) * mapSize + x;
    unsigned int yNext = (y + 1) * mapSize + x;
    float xPrevHeight = 0, xNextHeight = 0, yPrevHeight = 0, yNextHeight = 0;

    vertex[0] = ((float)x) * unitSize;
    vertex[2] = ((float)y) * unitSize;
    vertex[1] = height[center];

    xPrevHeight = x ? height[center - 1] : (2 * height[center] - height[center + 1]);
    xNextHeight = (x < mapSize - 1) ? height[center + 1] : (2 * height[center] - height[center - 1]);
    yPrevHeight = y ? height[yPrev] : (2 * height[center] - height[yNext]);
    yNextHeight = (y < mapSize - 1) ? height[yNext] : (2 * height[center] - height[yPrev]);

    normal[2] = xNextHeight - xPrevHeight;
    normal[0] = yNextHeight - yPrevHeight;
    normal[1] = 2;
    normalize3(normal);
}

struct Map* map_new(unsigned int size, unsigned int cellSize, float unitSize, float roughness) {
    struct Mesh mesh;
    struct Geometry* geometry;
    struct Material* material = NULL;
    struct Map* map;
    float* pondmap;
    struct Node* cell;
    unsigned int i, x, y, cx, cy;
    unsigned int child, nbCellSize = size / cellSize;
    GLuint texture;
    int ok = 1;

    mesh.flags = MESH_NORMALS | MESH_TEXCOORDS;
    mesh.numIndices = 0;
    mesh.indices = NULL;
    mesh.numVertices = 6 * cellSize * cellSize;
    mesh.vertices = malloc(MESH_SIZEOF_VERTICES(&mesh));

    if ((map = malloc(sizeof(struct Map)))) {
        map->size = size++;
        map->height = malloc(size * size * sizeof(float));
        map->master = malloc((nbCellSize * nbCellSize + 1) * sizeof(*map->master));
        map->nbCellSize = nbCellSize;
        map->unitSize = unitSize;
        pondmap = malloc(size * size * sizeof(float));
    }
    if ((texture = texture_load_from_png(SL3D_DATA_PATH "/textures/snow.png"))) {
        phong_material_params_init(&map->phongParams);
        material_param_set_vec3_texture(&map->phongParams.ambient, texture);
        material_param_set_vec3_texture(&map->phongParams.diffuse, texture);
        material_param_set_vec3_texture(&map->phongParams.specular, texture);
        material_param_set_float_constant(&map->phongParams.shininess, 1.0);
        material = sl3d_material_new(mesh.flags, &map->phongParams);
    }

    if (!mesh.vertices || !map || !map->height || !map->master || !pondmap || !material || !texture) {
        free(mesh.vertices);
        if (map) {
            free(map->height);
            free(map->master);
            free(map);
            free(pondmap);
        }
        free(material);
        if (texture) glDeleteTextures(1, &texture);
        return NULL;
    }

#if 0
    for (y = 0; y < size; y++) {
        for (x = 0; x < size; x++) {
            map->height[y * size + x] = x;
        }
    }
#endif

    gen_perlin(pondmap, size, size/2, roughness, 1.5, 0);
    gen_diamond_square(map->height, pondmap, size);
    free(pondmap);

    node_init(map->master);

    for (i = 0; i < mesh.numVertices; i++) {
        float* uv = mesh.vertices + (i * MESH_FLOATS_PER_VERTEX(&mesh) + 6);
        switch (i % 6) {
            case 0: uv[0] = 0; uv[1] = 0; break;
            case 1: uv[0] = 0; uv[1] = 1; break;
            case 2: uv[0] = 1; uv[1] = 0; break;
            case 3: uv[0] = 1; uv[1] = 0; break;
            case 4: uv[0] = 0; uv[1] = 1; break;
            case 5: uv[0] = 1; uv[1] = 1; break;
        }
    }

    child = 1;
    for (y = 0; y < map->size; y += cellSize) {
        for (x = 0; x < map->size; x += cellSize) {
            cell = map->master + child;
            for (cy = 0; cy < cellSize; cy++) {
                for (cx = 0; cx < cellSize; cx++) {
                    for (i = 0; i < 6; i++) {
                        unsigned int dx = (i == 2 || i == 3 || i == 5);
                        unsigned int dy = (i == 1 || i == 4 || i == 5);
                        float* v = mesh.vertices + (6 * (cy * cellSize + cx) + i) * MESH_FLOATS_PER_VERTEX(&mesh);
                        set_cell_data(x + cx + dx, y + cy + dy, size, unitSize, map->height, v, v + 3);
                    }
                }
            }
            node_init(cell);
            if (ok) {
                ok = (geometry = malloc(sizeof(*geometry)))
                    && (geometry->vertexArray = vertex_array_new(&mesh))
                    && node_add_child(map->master, cell);
                if (ok) {
                    geometry->material = material;
                    node_set_geometry(cell, geometry);
                } else {
                    if (geometry) {
                        vertex_array_free(geometry->vertexArray);
                        free(geometry);
                    }
                }
            }
            child++;
        }
    }
    mesh_free(&mesh);
    if (!ok) {
        free(map->height);
        free(map->master);
        free(map);
        free(material);
        if (texture) glDeleteTextures(1, &texture);
        return NULL;
    }

    return map;
}

void map_free(struct Map* map) {
    if (map) {
        struct Material* material = map->master[1].data.geometry->material;
        GLuint texture = ((struct PhongMaterialParams*)material->params)->ambient.value.texture;
        unsigned int i;
        if (texture) glDeleteTextures(1, &texture);
        free(material);
        for (i = 0; i < map->master->nbChildren; i++) {
            vertex_array_free(map->master->children[i]->data.geometry->vertexArray);
            free(map->master->children[i]->data.geometry);
        }
        nodes_free(map->master, NULL);
        free(map->master);
        free(map->height);
        free(map);
    }
}

float map_get_height(const struct Map* map, float x, float y) {
    unsigned int size = map->size + 1;
    unsigned int xBase = (int)(x /= map->unitSize);
    unsigned int yBase = (int)(y /= map->unitSize);
    float dx = x - ((float)xBase);
    float dy = y - ((float)yBase);
    float dzx, dzy, hBase;

    if (x <= 0 || x >= size || y <= 0 || y >= size) return FLT_MAX;

    if (dx + dy < 1.0) {
        hBase = map->height[yBase * size + xBase];
        dzx = map->height[yBase * size + xBase + 1] - hBase;
        dzy = map->height[(yBase + 1) * size + xBase] - hBase;
    } else {
        dx = 1 - dx;
        dy = 1 - dy;
        hBase = map->height[(yBase + 1) * size + xBase + 1];
        dzx = map->height[(yBase + 1) * size + xBase] - hBase;
        dzy = map->height[yBase * size + xBase + 1] - hBase;
    }

    return hBase + dx * dzx + dy * dzy;
}

int map_get_normal(const struct Map* map, float x, float y, Vec3 normal) {
    Vec3 u, v;
    unsigned int size = map->size + 1;
    unsigned int xBase = (int)(x /= map->unitSize);
    unsigned int yBase = (int)(y /= map->unitSize);
    float dx = x - ((float)xBase);
    float dy = y - ((float)yBase);

    if (x <= 0 || x >= size || y <= 0 || y >= size) return 0;

    if (dx + dy < 1.0) {
        u[0] = 1;
        u[2] = 0;
        u[1] = map->height[yBase * size + xBase + 1] - map->height[yBase * size + xBase];
        v[0] = 0;
        v[2] = 1;
        v[1] = map->height[(yBase + 1) * size + xBase] - map->height[yBase * size + xBase];
    } else {
        u[0] = -1;
        u[2] = 0;
        u[1] = map->height[(yBase + 1) * size + xBase] - map->height[(yBase + 1) * size + xBase + 1];
        v[0] = 0;
        v[2] = -1;
        v[1] = map->height[yBase * size + xBase + 1] - map->height[(yBase + 1) * size + xBase + 1];
    }
    cross3(normal, v, u);
    normalize3(normal);

    return 1;
}
