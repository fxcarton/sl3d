#include <stdlib.h>
#include <3dmr/mesh/box.h>
#include "material.h"

#define SL3D_SHADERS_PATH SL3D_DATA_PATH "/shaders"

static int fogEnabled = 0;

void material_enable_fog(void) {
    fogEnabled = 1;
}

GLuint vertex_shader_fog(enum MeshFlags flags) {
    static const char* includePaths[] = {TDMR_SHADERS_PATH, SL3D_SHADERS_PATH};
    const char* defines[2 * 3];
    unsigned int numDefines = 0;

    if (flags & MESH_NORMALS) {
        defines[2 * numDefines] = "HAVE_NORMAL";
        defines[2 * numDefines++ + 1] = NULL;
    }
    if (flags & MESH_TEXCOORDS) {
        defines[2 * numDefines] = "HAVE_TEXCOORD";
        defines[2 * numDefines++ + 1] = NULL;
    }
    if (flags & MESH_TANGENTS) {
        defines[2 * numDefines] = "HAVE_TANGENT";
        defines[2 * numDefines++ + 1] = NULL;
    }
    return shader_find_compile("fog.vert", GL_VERTEX_SHADER, includePaths, 2, defines, numDefines);
}

GLuint fog_phong_shader_new(const struct PhongMaterialParams* params) {
    static const char* includePaths[] = {TDMR_SHADERS_PATH, SL3D_SHADERS_PATH};
    static const char* defines[2 * (5 + ALPHA_MAX_DEFINES)];
    unsigned int numDefines = 0;

    if (params->normalMap) {
        defines[2 * numDefines] = "NORMALMAP";
        defines[2 * numDefines++ + 1] = NULL;
    }
    if (params->ambient.mode == MAT_PARAM_TEXTURED) {
        defines[2 * numDefines] = "AMBIENT_TEXTURED";
        defines[2 * numDefines++ + 1] = NULL;
    }
    if (params->diffuse.mode == MAT_PARAM_TEXTURED) {
        defines[2 * numDefines] = "DIFFUSE_TEXTURED";
        defines[2 * numDefines++ + 1] = NULL;
    }
    if (params->specular.mode == MAT_PARAM_TEXTURED) {
        defines[2 * numDefines] = "SPECULAR_TEXTURED";
        defines[2 * numDefines++ + 1] = NULL;
    }
    if (params->shininess.mode == MAT_PARAM_TEXTURED) {
        defines[2 * numDefines] = "SHININESS_TEXTURED";
        defines[2 * numDefines++ + 1] = NULL;
    }
    alpha_set_defines(&params->alpha, defines, &numDefines);
    return shader_find_compile("fog.frag", GL_FRAGMENT_SHADER, includePaths, 2, defines, numDefines);
}

struct Material* sl3d_material_new(enum MeshFlags mflags, const struct PhongMaterialParams* params) {
    struct Material* m = NULL;
    GLuint shaders[2];
    shaders[0] = (fogEnabled ? vertex_shader_fog : vertex_shader_standard)(mflags);
    shaders[1] = (fogEnabled ? fog_phong_shader_new : phong_shader_new)(params);
    if (shaders[0] && shaders[1]) m = material_new_from_shaders(shaders, 2, phong_load, (void*)params, GL_FILL);
    if (shaders[0]) glDeleteShader(shaders[0]);
    if (shaders[1]) glDeleteShader(shaders[1]);
    return m;
}

int fog_skybox_create(struct Skybox* skybox) {
    static const char* includePaths[] = {TDMR_SHADERS_PATH, SL3D_SHADERS_PATH};
    struct Mesh box;
    GLuint prog;

    if (!(prog = shader_find_compile_link_vertfrag("skybox.vert", "fog_skybox.frag", includePaths, 2, NULL, 0, NULL, 0))) return 0;
    if (make_box(&box, 1, 1, 1)) {
        vertex_array_gen(&box, &skybox->vertexArray);
        mesh_free(&box);
    }
    skybox->material.load = 0;
    skybox->material.params = 0;
    skybox->material.program = prog;
    skybox->material.polygonMode = GL_FILL;
    skybox->texture = 0;

    return 1;
}

struct Skybox* fog_skybox_new(void) {
    struct Skybox* res;

    if ((res = malloc(sizeof(*res)))) {
        if (fog_skybox_create(res)) {
            return res;
        }
        free(res);
    }
    return NULL;
}
