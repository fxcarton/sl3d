#include <3dmr/render/viewer.h>
#include <3dmr/scene/scene.h>
#include <3dmr/skybox.h>
#include "map.h"
#include "sled.h"
#include "dog.h"

#ifndef GAME_H
#define GAME_H

struct Params {
    int fog;

    float forestDensity;
    float forestCompactness;

    float terrainRoughness;
};

#define NB_DOGS 3

struct Game {
    struct Scene scene;
    struct Viewer* viewer;
    struct Skybox* skybox;
    GLuint skyboxTex;
    struct Map* map;
    struct Node* forest;
    struct Node* flag;
    struct Sled* sled;
    struct Dog* dog[NB_DOGS];
    struct Node* minimap;
    struct Node* mapSledPoint;
    struct Camera camera;
    int running;
    struct Geometry *fullTree, *reducedTree;
    size_t numTrees;
};

int game_init(struct Game* game, const struct Params* params);
void game_free(struct Game* game);

void game_update(struct Game* game);

#endif
