#include "map.h"
#include <3dmr/material/phong.h>
#include <3dmr/math/linear_algebra.h>
#include <3dmr/math/quaternion.h>
#include <3dmr/scene/node.h>

#ifndef SLED_H
#define SLED_H

enum SledUpdateError {
    SLED_NO_ERROR,
    SLED_OUT_OF_MAP,
    SLED_TOO_MUCH_ROTATION,
    SLED_COLLISION_WITH_TREE
};

struct Sled {
    struct Node node;
    struct Geometry geometry;
    struct PhongMaterialParams phongParams;
    Vec3 speed;
    Quaternion angularSpeed;
    Vec3 traction;
    float brake;
    
    int isFlying, isGliding;
};


struct Sled* sled_new(void);
void sled_free(struct Sled* sled);

enum SledUpdateError sled_update(struct Sled* sled, struct Map* map, float step);

#endif
