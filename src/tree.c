#include <stdio.h>
#include <stdlib.h>
#include <3dmr/material/phong.h>
#include <3dmr/mesh/obj.h>
#include <3dmr/render/texture.h>
#include <3dmr/scene/node.h>
#include "tree.h"
#include "material.h"

struct Geometry* tree_geometry(void) {
    struct Mesh mesh;
    struct Geometry* geom;
    struct PhongMaterialParams* phongParams;
    GLuint texture;

    if (!(geom = malloc(sizeof(*geom) + sizeof(*phongParams)))) return NULL;
    phongParams = (void*)(geom + 1);
    geom->vertexArray = NULL;
    geom->material = NULL;

    if ((texture = texture_load_from_png(SL3D_DATA_PATH "/textures/pine.png"))) {
        phong_material_params_init(phongParams);
        material_param_set_vec3_texture(&phongParams->ambient, texture);
        material_param_set_vec3_texture(&phongParams->diffuse, texture);
        material_param_set_vec3_texture(&phongParams->specular, texture);
        material_param_set_float_constant(&phongParams->shininess, 1);
    }
    if (make_obj(&mesh, SL3D_DATA_PATH "/models/tree.obj", 0, 1, 1)) {
        geom->vertexArray = vertex_array_new(&mesh);
        mesh_free(&mesh);
    }

    if (!geom->vertexArray || !texture || !(geom->material = sl3d_material_new(geom->vertexArray->flags, phongParams))) {
        if (texture) glDeleteTextures(1, &texture);
        vertex_array_free(geom->vertexArray);
        free(geom);
        return NULL;
    }

    return geom;
}

void tree_geometry_free(struct Geometry* geom) {
    if (geom) {
        GLuint texture = ((struct PhongMaterialParams*)geom->material->params)->ambient.value.texture;
        if (texture) glDeleteTextures(1, &texture);
        free(geom->material);
        vertex_array_free(geom->vertexArray);
        free(geom);
    }
}

struct Geometry* tree_reduced_geometry(const struct Geometry* geom) {
    struct Mesh reduced;
    float vertices[48], xmin, xmax, ymin, ymax;
    struct Geometry* greduced;

    if (!(greduced = malloc(sizeof(*greduced)))) return NULL;
    xmin = geom->vertexArray->boundingBox[0][0];
    xmax = geom->vertexArray->boundingBox[1][0];
    ymin = geom->vertexArray->boundingBox[0][1];
    ymax = geom->vertexArray->boundingBox[1][1];
    reduced.numVertices = 6;
    reduced.numIndices = 0;
    reduced.vertices = vertices;
    reduced.indices = NULL;
    reduced.flags = MESH_NORMALS | MESH_TEXCOORDS;
    vertices[0] = 0; vertices[1] = ymax; vertices[2] = 0;
    vertices[8] = xmin; vertices[9] = ymin; vertices[10] = 0;
    vertices[16] = xmax; vertices[17] = ymin; vertices[18] = 0;
    vertices[24] = 0; vertices[25] = ymax; vertices[26] = 0;
    vertices[32] = 0; vertices[33] = ymin; vertices[34] = xmin;
    vertices[40] = 0; vertices[41] = ymin; vertices[42] = xmax;

    vertices[3] = 0; vertices[4] = 0; vertices[5] = 1;
    vertices[11] = 0; vertices[12] = 0; vertices[13] = 1;
    vertices[19] = 0; vertices[20] = 0; vertices[21] = 1;
    vertices[27] = 1; vertices[28] = 0; vertices[29] = 0;
    vertices[35] = 1; vertices[36] = 0; vertices[37] = 0;
    vertices[43] = 1; vertices[44] = 0; vertices[45] = 0;

    vertices[6] = 0.856; vertices[7] = 0.045;
    vertices[14] = 0.712; vertices[15] = 0.973;
    vertices[22] = 1; vertices[23] = 1;
    vertices[30] = 0.856; vertices[31] = 0.045;
    vertices[38] = 0.712; vertices[39] = 0.973;
    vertices[46] = 1; vertices[47] = 1;

    if (!(greduced->vertexArray = vertex_array_new(&reduced))) {
        free(greduced);
        return NULL;
    }
    greduced->material = geom->material;

    return greduced;
}

void tree_reduced_geometry_free(struct Geometry* geom) {
    if (geom) {
        vertex_array_free(geom->vertexArray);
        free(geom);
    }
}
