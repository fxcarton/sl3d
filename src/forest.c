#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <3dmr/scene/node.h>

#include "forest.h"
#include "rand_tools.h"

struct Node* forest_generate(struct Map* map, float density, float compactness, int forestSize, size_t* numTrees) {
    struct Node *nodes, *cur;
    float* densityMap;
    size_t n;
    unsigned int cellSize = map->size / map->nbCellSize;
    unsigned int i, j, k;

    if (!(densityMap = malloc(sizeof(float) * map->size * map->size))) {
        return NULL;
    }
    n = map->nbCellSize * map->nbCellSize * cellSize * cellSize;
    if (numTrees) *numTrees = n;
    if (!(nodes = malloc(n * sizeof(*nodes)))) {
        free(densityMap);
        return NULL;
    }

    gen_perlin(densityMap, map->size, forestSize, density, compactness, 0);

    cur = nodes;
    for (i = 0; i < map->nbCellSize * map->nbCellSize; i++) {
        for (j = 0; j < cellSize; j++) {
            for (k = 0; k < cellSize; k++) {
                unsigned int xind = k + (i % map->nbCellSize) * cellSize;
                unsigned int yind = j + (i / map->nbCellSize) * cellSize;
                float proba = densityMap[yind * map->size + xind];
                float x = (xind + rand_float(0, 1)) * map->unitSize;
                float y = (yind + rand_float(0, 1)) * map->unitSize;
                float scale = proba < 0.2 ? 0.2 : proba;
                Vec3 scales;

                if (bernoulli(proba)) {
                    Vec3 translation;
                    node_init(cur);

                    translation[0] = x;
                    translation[2] = y;
                    translation[1] = map_get_height(map, translation[0], translation[2]);

                    node_translate(cur, translation);
                    scales[0] = scales[1] = scales[2] = scale;
                    node_set_scale(cur, scales);

                    node_add_child(map->master->children[i], cur);
                    cur++;
                }
            }
        }
    }
    while (cur < nodes + n) {
        node_init(cur++);
    }

    free(densityMap);
    return nodes;
}
