#include <stdio.h>
#include <stdlib.h>

#include <3dmr/material/phong.h>
#include <3dmr/mesh/obj.h>
#include <3dmr/render/texture.h>
#include <3dmr/scene/node.h>
#include "flag.h"
#include "rand_tools.h"
#include "material.h"

struct Node* flag_new(const struct Map* map) {
    struct Mesh mesh;
    struct Node* node;
    struct Geometry* geom;
    struct PhongMaterialParams* phongParams;
    Vec3 flagPos;
    GLuint texture;

    flagPos[0] = rand_float(0, map->size * map->unitSize);
    flagPos[2] = rand_float(0, map->size * map->unitSize);
    flagPos[1] = map_get_height(map, flagPos[0], flagPos[2]);
    printf("Flag at position: %f - %f\n", flagPos[0], flagPos[2]);

    if (!(node = malloc(sizeof(*node) + sizeof(*geom) + sizeof(*phongParams)))) return 0;
    geom = (void*)(node + 1);
    phongParams = (void*)(geom + 1);
    node_init(node);
    geom->material = NULL;
    geom->vertexArray = NULL;
    phongParams->ambient.value.texture = 0;

    if (make_obj(&mesh, SL3D_DATA_PATH "/models/flag.obj", 0, 1, 1)) {
        geom->vertexArray = vertex_array_new(&mesh);
        mesh_free(&mesh);
    }
    if ((texture = texture_load_from_png(SL3D_DATA_PATH "/textures/flag.png"))) {
        phong_material_params_init(phongParams);
        material_param_set_vec3_texture(&phongParams->ambient, texture);
        material_param_set_vec3_texture(&phongParams->diffuse, texture);
        material_param_set_vec3_texture(&phongParams->specular, texture);
        material_param_set_float_constant(&phongParams->shininess, 1);
    }
    if (geom->vertexArray) {
        geom->material = sl3d_material_new(geom->vertexArray->flags, phongParams);
    }
    if (!geom->material) {
        flag_free(node);
        return NULL;
    }

    node_set_geometry(node, geom);
    node_translate(node, flagPos);
    return node;
}

void flag_free(struct Node* node) {
    if (node) {
        struct Geometry* geom = (void*)(node + 1);
        struct PhongMaterialParams* phongParams = (void*)(geom + 1);
        if (phongParams->ambient.value.texture) glDeleteTextures(1, &phongParams->ambient.value.texture);
        free(geom->material);
        vertex_array_free(geom->vertexArray);
        free(node);
    }
}
