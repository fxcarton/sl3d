#include <3dmr/scene/node.h>
#include "map.h"

#ifndef FLAG_H
#define FLAG_H

struct Node* flag_new(const struct Map* map);
void flag_free(struct Node* node);

#endif
